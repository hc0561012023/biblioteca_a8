class Libro:
    def __init__(self, titulo, autor, ejemplares):
        self.titulo = titulo
        self.autor = autor
        self.ejemplares = ejemplares

class Biblioteca:
    def __init__(self):
        self.libros = {
            "Libro 1": Libro("Libro 1", "Autor 1", 5),
            "Libro 2": Libro("Libro 2", "Autor 2", 3),
            "Libro 3": Libro("Libro 3", "Autor 3", 2),
            "Libro 4": Libro("Libro 4", "Autor 4", 4),
            "Libro 5": Libro("Libro 5", "Autor 5", 1)
        }

    def agregar_libro(self, titulo, autor, ejemplares):
        libro = Libro(titulo, autor, ejemplares)
        self.libros[titulo] = libro
        print(f"El libro '{titulo}' ha sido agregado a la s.")

    def tomar_libro_prestado(self, titulo):
        libro = self.libros.get(titulo)
        if libro and libro.ejemplares > 0:
            libro.ejemplares -= 1
            print(f"Has tomado prestado el libro '{titulo}'.")
        else:
            print("El libro no está disponible.")

    def devolver_libro(self, titulo):
        libro = self.libros.get(titulo)
        if libro:
            libro.ejemplares += 1
            print(f"Has devuelto el libro '{titulo}'.")
        else:
            print("El libro no está registrado en la biblioteca.")

    def consultar_libros_disponibles(self):
        print("Libros disponibles en la biblioteca:")
        for libro in self.libros.values():
            print(f"Título: {libro.titulo}")
            print(f"Autor: {libro.autor}")
            print(f"Ejemplares disponibles: {libro.ejemplares}")
            print()

    def mostrar_menu(self):
        print("BIENVENIDOS AL SISTEMA DE BIBLIOTECA.") 
        print("Seleccione la opción que desea:")    
        print("1. Agregar Libro")
        print("2. Tomar Libro Prestado")
        print("3. Devolver Libro")
        print("4. Consultar Libros Disponibles")
        print("5. Salir")

    def ejecutar(self):
        while True:
            self.mostrar_menu()
            opcion = input("Ingrese el número de la opción que desea seleccionar: ")

            if opcion == "1":
                titulo = input("Ingrese el título del libro: ")
                autor = input("Ingrese el autor del libro: ")
                ejemplares = int(input("Ingrese la cantidad de ejemplares disponibles: "))
                self.agregar_libro(titulo, autor, ejemplares)
            elif opcion == "2":
                titulo = input("Ingrese el título del libro que desea tomar prestado: ")
                self.tomar_libro_prestado(titulo)
            elif opcion == "3":
                titulo = input("Ingrese el título del libro que desea devolver: ")
                self.devolver_libro(titulo)
            elif opcion == "4":
                self.consultar_libros_disponibles()
            elif opcion == "5":
                print("Gracias por utilizar la biblioteca. ¡Hasta luego!")
                break
            else:
                print("Opción inválida. Por favor, seleccione una opción válida.")

biblioteca = Biblioteca()

biblioteca.agregar_libro("El Señor de los Anillos", "J.R.R. Tolkien", 3)
biblioteca.agregar_libro("1984", "George Orwell", 5)
biblioteca.agregar_libro("Cien años de soledad", "Gabriel García Márquez", 2)
biblioteca.agregar_libro("Harry Potter y la piedra filosofal", "J.K. Rowling", 4)
biblioteca.agregar_libro("Don Quijote de la Mancha", "Miguel de Cervantes", 1)

biblioteca.ejecutar()
